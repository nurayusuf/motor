// JavaScript Document
$(document).ready(function(){

	$.ajax({  
	type: "POST", 
	url: "../include/permissions.php", 
	data: "view=" + "view", 
	success: function(output){
		if(output == 'Y') { $(".view").show(); } else { $(".view").hide(); }
	}
	}); // End

	$.ajax({  
	type: "POST", 
	url: "../include/permissions.php", 
	data: "edit=" + "edit", 
	success: function(output){
		if(output == 'Y') { $(".edit").show(); } else { $(".edit").hide(); }
	}
	}); // End

	$.ajax({  
	type: "POST", 
	url: "../include/permissions.php", 
	data: "delete=" + "delete", 
	success: function(output){
		if(output == 'Y') { $(".delete").show(); } else { $(".delete").hide(); }
	}
	}); // End

	$.ajax({  
	type: "POST", 
	url: "../include/permissions.php", 
	data: "reprint=" + "reprint", 
	success: function(output){
		if(output == 'Y') { $(".reprint").show(); } else { $(".reprint").hide(); }
	}
	}); // End

	$.ajax({  
	type: "POST", 
	url: "../include/permissions.php", 
	data: "restock=" + "restock", 
	success: function(output){
		if(output == 'Y') { $(".restock").show(); } else { $(".restock").hide(); }
	}
	}); // End

	$.ajax({  
	type: "POST", 
	url: "../include/permissions.php", 
	data: "csv=" + "csv", 
	success: function(output){
		if(output == 'Y') { $(".csv").show(); } else { $(".csv").hide(); }
	}
	}); // End

	$.ajax({  
	type: "POST", 
	url: "../include/permissions.php", 
	data: "pdf=" + "pdf", 
	success: function(output){
		if(output == 'Y') { $(".pdf").show(); } else { $(".pdf").hide(); }
	}
	}); // End

	$.ajax({  
	type: "POST", 
	url: "../include/permissions.php", 
	data: "t_sales_window=" + "t_sales_window", 
	success: function(output){
		if(output == 'Y') { $(".t_sales_window").show(); } else { $(".t_sales_window").hide(); }
	}
	}); // End

	$.ajax({  
	type: "POST", 
	url: "../include/permissions.php", 
	data: "t_products=" + "t_products", 
	success: function(output){
		if(output == 'Y') { $(".t_products").show(); } else { $(".t_products").hide(); }
	}
	}); // End

	$.ajax({  
	type: "POST", 
	url: "../include/permissions.php", 
	data: "new_product=" + "new_product", 
	success: function(output) {
		if(output == 'Y') {
			$(".new_product").show();
		} else {
			var pageName = "new_product.php";
			var currentPage = document.location.pathname.match(/[^\/]+$/)[0];
			if(currentPage === pageName) {
				window.location.replace("logout.php");
			} else {
				$(".new_product").hide();
			}
		}
	}
	}); // End

	$.ajax({  
	type: "POST", 
	url: "../include/permissions.php", 
	data: "all_products=" + "all_products", 
	success: function(output) {
		if(output == 'Y') {
			$(".all_products").show();
		} else {
			var pageName = "all_products.php";
			var currentPage = document.location.pathname.match(/[^\/]+$/)[0];
			if(currentPage === pageName) {
				window.location.replace("logout.php");
			} else {
				$(".all_products").hide();
			}
		}
	}
	}); // End

	$.ajax({  
	type: "POST", 
	url: "../include/permissions.php", 
	data: "low_stocks=" + "low_stocks", 
	success: function(output) {
		if(output == 'Y') {
			$(".low_stocks").show();
		} else {
			var pageName = "low_stocks.php";
			var currentPage = document.location.pathname.match(/[^\/]+$/)[0];
			if(currentPage === pageName) {
				window.location.replace("logout.php");
			} else {
				$(".low_stocks").hide();
			}
		}
	}
	}); // End

	$.ajax({  
	type: "POST", 
	url: "../include/permissions.php", 
	data: "t_sales=" + "t_sales", 
	success: function(output){
		if(output == 'Y') { $(".t_sales").show(); } else { $(".t_sales").hide(); }
	}
	}); // End

	$.ajax({  
	type: "POST", 
	url: "../include/permissions.php", 
	data: "search_sales=" + "search_sales", 
	success: function(output) {
		if(output == 'Y') {
			$(".search_sales").show();
		} else {
			var pageName = "search_sales.php";
			var currentPage = document.location.pathname.match(/[^\/]+$/)[0];
			if(currentPage === pageName) {
				window.location.replace("logout.php");
			} else {
				$(".search_sales").hide();
			}
		}
	}
	}); // End

	$.ajax({  
	type: "POST", 
	url: "../include/permissions.php", 
	data: "todays_sales=" + "todays_sales", 
	success: function(output) {
		if(output == 'Y') {
			$(".todays_sales").show();
		} else {
			var pageName = "todays_sales.php";
			var currentPage = document.location.pathname.match(/[^\/]+$/)[0];
			if(currentPage === pageName) {
				window.location.replace("logout.php");
			} else {
				$(".todays_sales").hide();
			}
		}
	}
	}); // End

	$.ajax({  
	type: "POST", 
	url: "../include/permissions.php", 
	data: "all_sales=" + "all_sales", 
	success: function(output) {
		if(output == 'Y') {
			$(".all_sales").show();
		} else {
			var pageName = "all_sales.php";
			var currentPage = document.location.pathname.match(/[^\/]+$/)[0];
			if(currentPage === pageName) {
				window.location.replace("logout.php");
			} else {
				$(".all_sales").hide();
			}
		}
	}
	}); // End

	$.ajax({  
	type: "POST", 
	url: "../include/permissions.php", 
	data: "t_accounts=" + "t_accounts", 
	success: function(output){
		if(output == 'Y') { $(".t_accounts").show(); } else { $(".t_accounts").hide(); }
	}
	}); // End

	$.ajax({  
	type: "POST", 
	url: "../include/permissions.php", 
	data: "search_accounts=" + "search_accounts", 
	success: function(output) {
		if(output == 'Y') {
			$(".search_accounts").show();
		} else {
			var pageName = "search_accounts.php";
			var currentPage = document.location.pathname.match(/[^\/]+$/)[0];
			if(currentPage === pageName) {
				window.location.replace("logout.php");
			} else {
				$(".search_accounts").hide();
			}
		}
	}
	}); // End

	$.ajax({  
	type: "POST", 
	url: "../include/permissions.php", 
	data: "cash_accounts=" + "cash_accounts", 
	success: function(output){
		if(output == 'Y') { $(".cash_accounts").show(); } else { $(".cash_accounts").hide(); }
	}
	}); // End

	$.ajax({  
	type: "POST", 
	url: "../include/permissions.php", 
	data: "bank_accounts=" + "bank_accounts", 
	success: function(output){
		if(output == 'Y') { $(".bank_accounts").show(); } else { $(".bank_accounts").hide(); }
	}
	}); // End

	$.ajax({  
	type: "POST", 
	url: "../include/permissions.php", 
	data: "t_settings=" + "t_settings", 
	success: function(output){
		if(output == 'Y') { $(".t_settings").show(); } else { $(".t_settings").hide(); }
	}
	}); // End

	$.ajax({  
	type: "POST", 
	url: "../include/permissions.php", 
	data: "setup_accounts=" + "setup_accounts", 
	success: function(output) {
		if(output == 'Y') {
			$(".setup_accounts").show();
		} else {
			var pageName = "setup_accounts.php";
			var currentPage = document.location.pathname.match(/[^\/]+$/)[0];
			if(currentPage === pageName) {
				window.location.replace("logout.php");
			} else {
				$(".setup_accounts").hide();
			}
		}
	}
	}); // End

	$.ajax({  
	type: "POST", 
	url: "../include/permissions.php", 
	data: "t_users=" + "t_users", 
	success: function(output){
		if(output == 'Y') { $(".t_users").show(); } else { $(".t_users").hide(); }
	}
	}); // End

	$.ajax({  
	type: "POST", 
	url: "../include/permissions.php", 
	data: "add_user=" + "add_user", 
	success: function(output) {
		if(output == 'Y') {
			$(".add_user").show();
		} else {
			var pageName = "add_user.php";
			var currentPage = document.location.pathname.match(/[^\/]+$/)[0];
			if(currentPage === pageName) {
				window.location.replace("logout.php");
			} else {
				$(".add_user").hide();
			}
		}
	}
	}); // End

	$.ajax({  
	type: "POST", 
	url: "../include/permissions.php", 
	data: "all_users=" + "all_users", 
	success: function(output) {
		if(output == 'Y') {
			$(".all_users").show();
		} else {
			var pageName = "all_users.php";
			var currentPage = document.location.pathname.match(/[^\/]+$/)[0];
			if(currentPage === pageName) {
				window.location.replace("logout.php");
			} else {
				$(".all_users").hide();
			}
		}
	}
	}); // End

	$.ajax({  
	type: "POST", 
	url: "../include/permissions.php", 
	data: "add_role=" + "add_role", 
	success: function(output) {
		if(output == 'Y') {
			$(".add_role").show();
		} else {
			var pageName = "add_role.php";
			var currentPage = document.location.pathname.match(/[^\/]+$/)[0];
			if(currentPage === pageName) {
				window.location.replace("logout.php");
			} else {
				$(".add_role").hide();
			}
		}
	}
	}); // End

	$.ajax({  
	type: "POST", 
	url: "../include/permissions.php", 
	data: "all_roles=" + "all_roles", 
	success: function(output) {
		if(output == 'Y') {
			$(".all_roles").show();
		} else {
			var pageName = "all_roles.php";
			var currentPage = document.location.pathname.match(/[^\/]+$/)[0];
			if(currentPage === pageName) {
				window.location.replace("logout.php");
			} else {
				$(".all_roles").hide();
			}
		}
	}
	}); // End

});