// JavaScript Document

// onkeypress="return numberOnly(event)"
function numberOnly(evt) {
	var theEvent = evt || window.event;
	var key = theEvent.keyCode || theEvent.which;
	key = String.fromCharCode(key);
	if (key.length == 0) return;
	var regex = /^[0-9\.\b]$/;
	if (!regex.test(key)) {
		theEvent.returnValue = false;
		if (theEvent.preventDefault) theEvent.preventDefault();
	}
}

function number_format(num) {
	return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

// Validation patterns
var username_pattern = /^[a-z]{1}[_0-9a-z]{5,}$/i;
var password_pattern = /^[a-zA-Z0-9]{5,}/; // Min 5 xters and a-z A-Z and 0-9 only
var varchar_pattern = /^[-\.\,\(\)\/\'\w\s\\\\]*$/i;
var number_pattern = /^[0-9\.]*$/i;
var mobile_pattern = /^((234)[1-9]{1}[0-9]{9}|(0){1}[1-9]{1}[0-9]{9})$/;

$(document).ready(function() {
	// Logout Confirmation
	$('#logout').on('click',function(e){
		e.preventDefault();
		swal({
			title: "",
			text: "Are you sure to logout?",
			type: "warning",
			showCancelButton: true,
			cancelButtonText: "NO",
			confirmButtonText: "Yes"
		}, function(isConfirm){
			if(isConfirm) {
				window.location.replace("logout.php");
			}
		});
	});

	// Username Validation
	$("#username").change(function() { 
		var val = $(this).val();
		if(val != '') {if(!username_pattern.test(val)){$(this).qtip({style:{classes:"qtip-red"},content:{text:"Must be at least 6 characters<br />Must start with an alphabet"},show:{ready:true}});}else{$(this).qtip("hide");}}
	});
	
	// Password & Answer Validation
	$("#current_password, #answer, #confirm_answer").change(function() { 
		var val = $(this).val();
		if(val != '') {$(this).qtip("hide");}
	});
	
	// Secret Answer Validation
	$("#secret_answer").change(function() { 
		var val = $(this).val();
		if(val != '') {$(this).qtip("hide");}
	});
	
	// Password Validation
	$("#new_password1, #new_password2").change(function() { 
		var val = $(this).val();
		var new1 = $('#new_password1').val();
		var new2 = $('#new_password2').val();
		if(val !== '') {
			if(!password_pattern.test(val)) {
				$(this).qtip({style:{classes:"qtip-red"},content:{text:"Must be at least 5 characters<br />Must be Alphabets & Numbers only"},show:{ready:true}});
			} else if((new1 !== '' && new2 !== '') && (new1 !== new2)) {
				$('#new_password1, #new_password2').qtip({style:{classes:"qtip-red"},content:{text:"Not Matching"},show:{ready:true}});
			} else {
				$('#new_password1, #new_password2').qtip("hide");
			}
		} else {
			$(this).qtip("hide");
		}
	});
	
	// Answer Validation
	$("#answer, #confirm_answer").change(function() { 
		var val = $(this).val();
		var answer1 = $('#answer').val();
		var answer2 = $('#confirm_answer').val();
		if(val !== '') { 
			if((answer1 !== '' && answer2 !== '') && (answer1 !== answer2)) {
				$('#answer, #confirm_answer').qtip({style:{classes:"qtip-red"},content:{text:"Not Matching"},show:{ready:true}});
			} else {
				$('#answer, #confirm_answer').qtip("hide");
			}
		} else {
			$(this).qtip("hide");
		}
	});
		
	// Varchar Validation
	$("#name, #remark").change(function() { 
		var val = $(this).val();
		if(val != '') {if(!varchar_pattern.test(val)){$(this).qtip({style:{classes:"qtip-red"},content:{text:"Invalid"},show:{ready:true}});}else{$(this).qtip("hide");}}
	});
	
	// Number Validation
	$("#amount, #percentage, #order_from, #order_to, #max_discount, #cash_received, #payment_ref, #unit_price, #quantity, #low_alert, #balance").change(function() { 
		var val = $(this).val();
		if(val != '') {if(!number_pattern.test(val)){$(this).qtip({style:{classes:"qtip-red"},content:{text:"Invalid"},show:{ready:true}});}else{$(this).qtip("hide");}}
	});

	// Mobile Validation
	$("#mobile").change(function() { 
		var val = $(this).val();
		if(val != '') {if(!mobile_pattern.test(val)){$(this).qtip({style:{classes:"qtip-red"},content:{text:"Invalid"},show:{ready:true}});}else{$(this).qtip("hide");}}
	});
	
	// Date Validation
	$("#from_date, #to_date").focus(function() { 
		var val = $(this).val();
		if(val != '') {$(this).qtip("hide");}
	});
	
	// Select List Validation
	$("#question_id").change(function() { var val = $(this).val(); if(val != ''){ $("#question_id_err").qtip("hide"); } });
	$("#role_id").change(function() { var val = $(this).val(); if(val != ''){ $("#role_id_err").qtip("hide"); } });
	$("#account_type").change(function() { var val = $(this).val(); if(val != ''){ $("#account_type_err").qtip("hide"); } });
	$("#product_id").change(function() { var val = $(this).val(); if(val != ''){ $("#product_id_err").qtip("hide"); } });

	// Radio button Validation
	$("input[name=discount_type]:radio").change(function() { if($(this).is(':checked')){ $("#discount_type_err").qtip("hide"); } });
});