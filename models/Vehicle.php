<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vehicle".
 *
 * @property int $id
 * @property string $product
 * @property string $color
 * @property string $year
 * @property int $adminid
 * @property string $created
 * @property int $ownerid
 *
 * @property User $admin
 * @property Owner $owner
 */
class Vehicle extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vehicle';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product', 'color', 'year', 'adminid', 'created', 'ownerid','plateno'], 'required'],
            [['year', 'created','adminid','ownerid'], 'safe'],
            // [['adminid', 'ownerid'], 'integer'],
            [['product'], 'string', 'max' => 255],
            [['color'], 'string', 'max' => 50],
            [['plateno'], 'string', 'max' => 20],
            [['adminid'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['adminid' => 'id']],
            [['ownerid'], 'exist', 'skipOnError' => true, 'targetClass' => Owner::className(), 'targetAttribute' => ['ownerid' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product' => 'Product',
            'color' => 'Color',
            'year' => 'Year',
            'adminid' => 'Adminid',
            'created' => 'Created',
            'ownerid' => 'Customer',
            'plateno' => 'Plate_No',
        ];
    }

    /**
     * Gets query for [[Admin]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAdmin()
    {
        return $this->hasOne(User::className(), ['id' => 'adminid']);
    }

    /**
     * Gets query for [[Owner]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(Owner::className(), ['id' => 'ownerid']);
    }
}
