<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AdminAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'vendor/qtip/jquery.qtip.min.css',
        'vendor/sweetalert/sweetalert.css',
        'vendor/fontawesome-free/css/all.min.css',
        'css/sb-admin-2.min.css',
        // 'vendor/datatables/dataTables.bootstrap4.min.css',
    ];
    public $js = [
      // 'vendor/jquery/jquery.min.js',
      'vendor/js/validation.js',
      'vendor/bootstrap/js/bootstrap.bundle.min.js',
      'vendor/jquery-easing/jquery.easing.min.js',
      'js/sb-admin-2.min.js',
      'vendor/sweetalert/sweetalert.min.js',
      'vendor/qtip/jquery.qtip.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
