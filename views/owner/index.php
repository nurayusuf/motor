<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OwnerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Owners';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="owner-index">

    <h3><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a('Create Owner', ['create'], ['class' => 'btn btn-success pull-right']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'names',
            'phone',
            'address',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {owner}',
                'buttons' => 
	                [
                        'view' => function ($url,$model,$key) {
                            return Html::a("<button class='btn btn-primary'>View</button>", $url);
                        },
                        'update' => function ($url,$model,$key) {
                            return Html::a("<button class='btn btn-warning'>Edit</button>", $url);
                        },
                        'owner' => function ($url,$model,$key) {
                            $url = ['vehicle/owner', 'id' => $model->id];
                            return Html::a("<button class='btn btn-info'>Vehicles</button>", $url);
                        },
	                ],
            ],
        ],
    ]); ?>


</div>
