<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Outer Row -->
<div class="row justify-content-center">

<div class="col-xl-10 col-lg-12 col-md-9">

  <div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
      <!-- Nested Row within Card Body -->
      <div class="row">
        <div class="col-lg-6 d-none d-lg-block bg-login-image">
          <img src="images/clr.jpg" class="img-responsive" style="width: 100%; height: 100%">
        </div>
        <div class="col-lg-6">
          <div class="p-5">
            <div class="text-center">
              <h1 class="h4 text-danger mb-4"> VEHICLE REGISTRATION SYSTEM </h1>
            </div>
            <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                'class' => 'user',
                'layout' => 'horizontal',
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"col-lg-5\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
                    'labelOptions' => ['class' => 'col-lg-2 control-label'],
                ],
            ]); ?>
              <div class="form-group">
              <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'class' => 'form-control form-control-user', 'placeholder'=>'Enter Email Or Username...','id'=>'user']) ?>
              </div>
              <div class="form-group">
              <?= $form->field($model, 'password')->passwordInput(['class' => 'form-control form-control-user', 'placeholder'=>'Enter Password...','id'=>'user']) ?>
              </div>
              <div class="form-group">
                <div class="custom-control custom-checkbox small">
                <?= $form->field($model, 'rememberMe')->checkbox([
                    'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
                ]) ?>
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-offset-4 col-lg-8">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-lg btn-primary', 'name' => 'login-button']) ?>
                </div>
            </div>
            
              <?php ActiveForm::end(); ?>
            <hr>
            <div class="text-center">
              <!-- <a class="small" href="forgot-password.html">Forgot Password?</a> -->
            </div>
            <div class="text-center"></div>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>

