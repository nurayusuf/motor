<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AdminAsset;

AdminAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body id="page-top">
<?php $this->beginBody() ?>

<div id="wrapper">
<?php if(!Yii::$app->user->isGuest) { ?>
<!-- Sidebar -->
<?php //include('sidebar.php') ?>
<!-- End of Sidebar -->
 <!-- Sidebar -->
 <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

<!-- Sidebar - Brand -->
<a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
  <div class="sidebar-brand-icon ">
    <i class="fas fa-truck"></i>
    <!-- <img src="img/logo1.jpeg" height="60"> -->
  </div>
  <div class="sidebar-brand-text mx-3"><b class="text-danger">VRS</b></div>
</a>
<!-- Divider -->
<hr class="sidebar-divider my-0">

<!-- Nav Item - Dashboard -->
  <li class="nav-item">
  <?=  Html::a('DASHBOARD', ['site/index',],['class'=>'nav-link']);  ?>
    <!-- <a class="nav-link" href="dashboard.php">
      <i class="fas fa-fw fa-home"></i>
      <span>Dashboard</span></a> -->
  </li>
  <hr class="sidebar-divider">
  <li class="nav-item">
  <?=  Html::a('OWNERS', ['owner/index',],['class'=>'nav-link']);  ?>
    <!-- <a class="nav-link" href="/owner/index">
        <i class="fas fa-fw fa-users"></i>
        <span>OWNERS</span></a> -->
  </li>
  <hr class="sidebar-divider">
  <li class="nav-item">
    <?=  Html::a('VEHICLES', ['vehicle/index',],['class'=>'nav-link']);  ?>
    <!-- <a class="nav-link" href="/vehicle/index">
    <i class="fas fa-fw fa-plus"></i>
    <span>VEHICLES</span></a> -->
  </li>
   <hr class="sidebar-divider">
  <li class="nav-item">
  <?=  Html::a('PRODUCTS', ['product/index',],['class'=>'nav-link']);  ?>
    <!-- <a class="nav-link" href="/product/index">
    <i class="fas fa-fw fa-plus"></i>
    <span>PRODUCTS</span></a> -->
  </li>
  <hr class="sidebar-divider">
  <li class="nav-item">
  <?=  Html::a('COLORS', ['color/index',],['class'=>'nav-link']);  ?>
    <!-- <a class="nav-link" href="/color/index">
    <i class="fas fa-fw fa-plus"></i>
    <span>COLORS</span></a> -->
  </li>
  <hr class="sidebar-divider">
  <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
        <i class="fas fa-fw fa-folder"></i>
        <span>Report</span>
      </a>
      <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <a class="collapse-item" href="report.php?token=general">General Report</a>
          <a class="collapse-item" href="report.php?token=customer">Customer Report</a>
          <a class="collapse-item" href="report.php?token=staff">Staff Report</a>
        </div>
      </div>
    </li>
<!-- Divider -->
<hr class="sidebar-divider d-none d-md-block">

<!-- Sidebar Toggler (Sidebar) -->
<div class="text-center d-none d-md-inline">
  <button class="rounded-circle border-0" id="sidebarToggle"></button>
</div>
</ul>
<!-- End of Sidebar -->
<?php } ?>

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

  <!-- Main Content -->
  <div id="content">
<?php if(!Yii::$app->user->isGuest) { ?>
    <!-- Topbar -->
    <?php //include('header.php') ?>
    <!-- End of Topbar -->
    <!-- Topbar -->
<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

<!-- Sidebar Toggle (Topbar) -->
<button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
  <i class="fa fa-bars"></i>
</button>

<!-- Topbar Search -->
<form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search" action="search.php" method="post">
  <div class="input-group">
    <input type="text" name="search" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
    <div class="input-group-append">
      <button class="btn btn-primary" type="submit" name="submit">
        <i class="fas fa-search fa-sm"></i>
      </button>
    </div>
  </div>
</form>

<!-- Topbar Navbar -->
<ul class="navbar-nav ml-auto">

  <!-- Nav Item - Search Dropdown (Visible Only XS) -->
  <li class="nav-item dropdown no-arrow d-sm-none">
    <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="fas fa-search fa-fw"></i>
    </a>
    <!-- Dropdown - Messages -->
    <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
      <form class="form-inline mr-auto w-100 navbar-search" action="search.php" method="post">
        <div class="input-group">
          <input type="text" name="search" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
          <div class="input-group-append">
            <button class="btn btn-primary" type="submit" name="submit">
              <i class="fas fa-search fa-sm"></i>
            </button>
          </div>
        </div>
      </form>
    </div>
  </li>

  <div class="topbar-divider d-none d-sm-block"></div>

  <!-- Nav Item - User Information -->
  <li class="nav-item dropdown no-arrow">
      <?php 
            echo '<li>'
             . Html::beginForm(['/site/logout'], 'post')
             . Html::submitButton(
                 'Logout (' . Yii::$app->user->identity->username . ')',
                 ['class' => 'btn btn-link logout']
             )
             . Html::endForm()
             . '</li>'
      ?>
  </li>

</ul>

</nav>
<!-- End of Topbar -->
<?php } ?>

    <!-- Begin Page Content -->
    <div class="container-fluid">
        <?= $content ?>
    </div>
    <!-- /.container-fluid -->

  <!-- End of Main Content -->

  <!-- Footer -->
  <?php //include('footer.php') ?>
  <!-- End of Footer -->
  <!-- Footer -->
  <footer class="sticky-footer bg-white">
    <div class="container my-auto">
      <div class="copyright text-center my-auto">
        <span>COPYRIGHT &copy; VRS NIG. LTD</span>
      </div>
    </div>
  </footer>
 <!-- End of Footer -->

</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
