<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Owner;
use app\models\Product;
use app\models\Color;


/* @var $this yii\web\View */
/* @var $model app\models\Vehicle */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vehicle-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'product')->dropDownList(
        ArrayHelper::map(Product::find()->all(),'name','name'),['prompt'=>'Select Product']
    ) ?>

    <?= $form->field($model, 'color')->dropDownList(
        ArrayHelper::map(Color::find()->all(),'name','name'),['prompt'=>'Select Color']
    ) ?>

    <?= $form->field($model, 'year')->textInput(['maxlength' => 4]) ?>

    <?= $form->field($model, 'plateno')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ownerid')->dropDownList(
        ArrayHelper::map(Owner::find()->all(),'id','names'),['prompt'=>'Select Customer']
    ) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
